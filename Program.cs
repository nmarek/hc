﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;

namespace HC
{

    class Matice
    {
        //počet rádků v matici
        public int PocetRadku { get; set; }
        //počet rsloupců v matici
        public int PocetSloupcu { get; set; }
        //pole pro ukládání čísel v matici
        public int[,] mat = new int[,] { };

        public Matice(int PocetRadku, int PocetSloupcu)
        {
            this.PocetRadku = PocetRadku;
            this.PocetSloupcu = PocetSloupcu;
        }


        public void VytvorMatici(int[,] G, int[,] H, KodSlovo c, Matice proKontrolu)
        {
            int[,] a = new int[c.ListProVstup.Count, proKontrolu.PocetRadku];
            for (int x = 0; x < c.ListProVstup.Count; x++)
            {
                for (int y = 0; y < proKontrolu.PocetRadku; y++)
                {
                    a[x, y] = H[y, x];
                }
            }

            for (int x = 0; x < this.PocetRadku; x++)
            {
                for (int y = 0; y < this.PocetSloupcu; y++)
                {
                    if (y < this.PocetRadku)
                    {
                        if (x == y)
                        {
                            G[x, y] = 1;
                        }
                        else
                        {
                            G[x, y] = 0;
                        }
                    }
                    else
                    {
                        G[x, y] = a[x, y - this.PocetRadku];
                    }
                }
            }
        }

        public void Vypis(int[,] NaseMatice)
        {
            for (int x = 0; x < this.PocetRadku; x++)
            {
                for (int y = 0; y < this.PocetSloupcu; y++)
                {
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write($"{NaseMatice[x, y]} ");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                Console.WriteLine();
            }
        }

        public void Prohodit(int[,] matice, int a, int b)
        {
            for (int x = 0; x < this.PocetRadku; x++)
            {
                int i = matice[x, a];
                matice[x, a] = matice[x, b];
                matice[x, b] = i;
            }
        }
    }

    //vstup uživatele - zvolíme písmeno,se kterým chceme pracovat
    class VstupUzivatele
    {

        public List<int> array1 = new List<int>() { };
        public char pismeno { get; set; }
        public string PrirazenyKod { get; set; }

        

        public VstupUzivatele(char pismeno, string PrirazenyKod)
        {
            this.pismeno = pismeno;
            this.PrirazenyKod = PrirazenyKod;
        }

        public void VytvorList()
        {
            foreach (char number in this.PrirazenyKod)
            {
                if (number == '0')
                {
                    this.array1.Add(0);

                }
                else
                {
                    this.array1.Add(1);
                }
            }

        }
    }


    class KodSlovo
    {
        public List<int> ListProVstup = new List<int>() { };
        public List<int> ListProVystup = new List<int>() { };
        public List<int> ListProSyndromSlova = new List<int>() { };
        public List<int> ListProZabezpeceni = new List<int>() { };

        public void ZabezpecKodSlovo(Matice m, int[,] G)
        {
            int countInColumn = 0;
            this.ListProVstup.Reverse();

            for (int i = 0; i < m.PocetSloupcu; i++)
            {
                for (int j = 0; j < m.PocetRadku; j++)
                {
                    countInColumn += (this.ListProVstup[j] * G[j, i]);
                    if (countInColumn == 2)
                    {
                        countInColumn = 0;
                    }
                }
                this.ListProZabezpeceni.Add(countInColumn);
                countInColumn = 0;
            }
        }

        public void VypisList(List<int> a)
        {
            foreach (int del in a)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($"{del}");
                Console.ForegroundColor = ConsoleColor.White;
            }
            
        }
  

        public void PoziceChyb(int PocetChyb, List<int> pozice)
        {
            if (PocetChyb >= 0 && PocetChyb <= 3)
            {
                while (pozice.Count != PocetChyb)
                {
                    Random x= new Random();
                    int a = x.Next(0, 6);

                    if (pozice.Count == 0)
                    {
                        pozice.Add(a);
                    }
                    else if (pozice.Count == 1 && a != pozice[0])
                    {
                        pozice.Add(a);
                    }
                    else if ( a != pozice[0] && a != pozice[1] && pozice.Count == 2 )
                    {
                        pozice.Add(a);
                    }
                }
            }

            //v případě, že zvolíme příliž mnoho chyb, zobrazí se chybová hláška
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERROR! Příliž mnoho chyb\n");
                Console.ForegroundColor = ConsoleColor.White;
  
            }
        }

        public void GenerujChybu(List<int> PoziceChyb)
        {
            foreach (int a in PoziceChyb)
            {
                if (this.ListProZabezpeceni[a] == 1)
                {
                    this.ListProZabezpeceni[a] = 0;
                }

                else
                {
                    this.ListProZabezpeceni[a] = 1;
                }
            }

        }

        public void OpravChybu(Matice oprava, int[,] H)
        {
            int[,] a = new int[oprava.PocetSloupcu, oprava.PocetRadku];

            for (int x = 0; x < oprava.PocetSloupcu; x++)
            {
                for (int y = 0; y < oprava.PocetRadku; y++)
                {
                    a[x, y] = H[y, x];
                }
            }

            int pocet = 0;

            for (int x = 0; x < oprava.PocetRadku; x++)
            {
                for (int y = 0; y < oprava.PocetSloupcu; y++)
                {
                    pocet += (ListProZabezpeceni[y] * a[y, x]);
                    if (pocet == 2)
                    {
                        pocet = 0;
                    }
                }

                this.ListProSyndromSlova.Add(pocet);
                pocet = 0;
            }

            foreach(int x in this.ListProSyndromSlova)
            {
                Console.WriteLine(x);
            }
          
        }


        public void Oprav(int PocetChyb)
        {
            if (PocetChyb == 1)
            {
                this.ListProSyndromSlova.Reverse();

                double NaPozici = 0;
                
                for (int x = 0; x < this.ListProSyndromSlova.Count; x++)
                {
                    if (this.ListProSyndromSlova[x] == 1)
                    {
                        NaPozici += Math.Pow(2, (double)x);
                    }
                }

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Chyba na pozici: {NaPozici} ");
                Console.ForegroundColor = ConsoleColor.White;

                for (int x = 0; x < this.ListProZabezpeceni.Count; x++)
                {
                    if (x == NaPozici - 1)
                    {
                        if (this.ListProZabezpeceni[x] == 0)
                        {
                            this.ListProZabezpeceni[x] = 1;
                        }
                        else
                        {
                            this.ListProZabezpeceni[x] = 0;
                        }
                    }
                } 
            }
             
        }

        public void InformacniBit(int kontrola)
        {
            List<double> x1 = new List<double>() { };

            for (int x = 0; x < kontrola; x++)
            {
                x1.Add(Math.Pow(2, x));
            }

            for (int x = 1; x < this.ListProZabezpeceni.Count + 1; x++)
            {
                int pocetY = 0;

                for (int y = 0; y < x1.Count; y++)
                {
                    if (x != x1[y])
                    {
                        ++pocetY;
                    }
                }

                if (pocetY == kontrola)
                {
                    this.ListProVystup.Add(ListProZabezpeceni[x - 1]);
                }
            }
        }

        public void PorovnejVstup(List<VstupUzivatele> vyber)
        {
            foreach (VstupUzivatele x in vyber)
            {
                x.VytvorList();
                int shoda = 0;

                for (int y = 0; y < x.array1.Count; y++)
                {
                    if (x.array1[y] == this.ListProVystup[y])
                    {
                        ++shoda;
                    }
                }

                if (shoda == this.ListProVystup.Count)
                {
                    
                    Console.WriteLine(x.pismeno);
                  
                    break;
                }
            }
        }

    }
    class Program
    {
        static void Main(string[] args)
        {

            //List, do kterého jsem nahrál možné vstupy
            List<VstupUzivatele> vstup = new List<VstupUzivatele>() { };
            //vstupy
            vstup.Add(new VstupUzivatele('A', "0000"));
            vstup.Add(new VstupUzivatele('B', "0001"));
            vstup.Add(new VstupUzivatele('C', "0010"));
            vstup.Add(new VstupUzivatele('D', "0011"));
            vstup.Add(new VstupUzivatele('E', "0100"));
            vstup.Add(new VstupUzivatele('F', "0101"));
            vstup.Add(new VstupUzivatele('G', "0110"));
            vstup.Add(new VstupUzivatele('H', "0111"));
            vstup.Add(new VstupUzivatele('I', "1000"));
            vstup.Add(new VstupUzivatele('J', "1001"));
            vstup.Add(new VstupUzivatele('K', "1010"));
            vstup.Add(new VstupUzivatele('L', "1011"));
            vstup.Add(new VstupUzivatele('M', "1100"));
            vstup.Add(new VstupUzivatele('N', "1101"));
            vstup.Add(new VstupUzivatele('O', "1110"));
            vstup.Add(new VstupUzivatele('P', "1111"));

            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("***** HAMMINGŮV KÓD *****");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("\nZadejte vstup v podobě písmene:");
            char vstupUzivatele = Console.ReadKey().KeyChar;
            Console.WriteLine("\n-----------------------\n");

            KodSlovo zdroj = new KodSlovo();


            foreach (VstupUzivatele moznost in vstup)
            {
                if (moznost.pismeno == vstupUzivatele)
                {
                    foreach (char V1 in moznost.PrirazenyKod)
                    {
                        if (V1 == '1')
                        {
                            zdroj.ListProVstup.Add(1);
                        }
                        else
                        {
                            zdroj.ListProVstup.Add(0);
                        }
                    }
                    break;
                }
            }

            zdroj.VypisList(zdroj.ListProVstup);
            Console.WriteLine("\n-----------------------\n");



            //Matice H dle zadání (Kontrolní matice).
            Matice a = new Matice(3, 7);
            a.mat = new int[,] { { 0, 0, 0, 1, 1, 1, 1 },
                                 { 0, 1, 1, 0, 0, 1, 1 },
                                 { 1, 0, 1, 0, 1, 0, 1 } };

            //Swaping columns for creating generating matrix
            a.Prohodit(a.mat, 0, 6);
            a.Prohodit(a.mat, 1, 5);
            a.Prohodit(a.mat, 3, 4);

            //Matice G dle zadání (Generující matice).
            Matice G = new Matice(zdroj.ListProVstup.Count, a.PocetSloupcu);
            G.mat = new int[G.PocetRadku, G.PocetSloupcu];
            G.VytvorMatici(G.mat, a.mat, zdroj, a);

           
            G.Prohodit(G.mat, 0, 6);
            G.Prohodit(G.mat, 1, 5);
            G.Prohodit(G.mat, 3, 4);

            a.Prohodit(a.mat, 0, 6);
            a.Prohodit(a.mat, 1, 5);
            a.Prohodit(a.mat, 3, 4);

            Console.WriteLine("Matice G - generující matice");
            G.Vypis(G.mat);
            Console.WriteLine("\n-----------------------\n");

            zdroj.ZabezpecKodSlovo(G, G.mat);
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("** Zabezpečené kódové slovo:");
            Console.ForegroundColor = ConsoleColor.White;
            zdroj.VypisList(zdroj.ListProZabezpeceni);

            Console.WriteLine("\n-----------------------\n");

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("** Počet chyb:");
            Console.ForegroundColor = ConsoleColor.White;
            int Volba = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("\n-----------------------\n");


            List<int> t = new List<int>() { };
            zdroj.PoziceChyb(Volba, t);
            zdroj.GenerujChybu(t);

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"** Zabezpečené slovo, ve kterém se nachází uživatelem zadaná chyba");
            Console.ForegroundColor = ConsoleColor.White;
            zdroj.VypisList(zdroj.ListProZabezpeceni);

            Console.WriteLine("\n-----------------------\n");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("** Matice H - kontrolní matice");
            Console.ForegroundColor = ConsoleColor.White;
            a.Vypis(a.mat);

            Console.WriteLine("\n-----------------------\n");

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("** Syndrom slova:");
            Console.ForegroundColor = ConsoleColor.White;
            zdroj.OpravChybu(a, a.mat);

            Console.WriteLine("\n-----------------------\n");

            //Oprava chyby
            zdroj.Oprav(Volba);
            zdroj.InformacniBit(a.PocetRadku);
            Console.WriteLine();

            Console.WriteLine("** Opravené přijaté kódové slovo: ");
            zdroj.VypisList(zdroj.ListProVystup);

            
            Console.WriteLine("\n-----------------------\n");

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("** Shoda! Našla se shoda pro vstup uživatele:  ");
            zdroj.PorovnejVstup(vstup);
            Console.ForegroundColor = ConsoleColor.Black;
        }
    }
}
